##  The **GOTM** model - compatible with WET

GOTM - the **G**eneral **O**cean **T**urbulence **M**odel is an ambitious name for a one-dimensional water column model for marine and limnological applications. It is coupled to a choice of traditional as well as state-of-the-art parameterisations for vertical turbulent mixing. The package consists of the FORTRAN source code, a number of idealised and realistic test cases, and a scientific documentation, all published under the GNU public license.

A comprehensive description including compilation instructions are given at the official [GOTM homepage](http://www.gotm.net/portfolio/software).

This particular version of GOTM is compatible with the Water Ecosystems Tool (WET), and includes code adaptions that enable hypsographic representation of the physical domain, relevant for lakes, reservoirs and lagoons, as well as a manipulation routine that enable virtual experiments that adds or removes quantities from any user-defined state variable at a given time and depth.
